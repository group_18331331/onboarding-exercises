<?php

// Adding headers for CORS
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");


require_once ('MysqliDb.php');

// API class definition
class API {
    private $db;

    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'p8');
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload) 
    {
        // Check if the payload is an array
        if (!is_array($payload)) {
            return $this->failedResponse("GET", "Invalid Payload Format");
        }

        // Check if 'id' is provided in the payload
        if (isset($payload['id'])) {
            $id = $payload['id'];

            // If 'id' is an array, fetch data for all provided IDs
            if (is_array($id)) {
                $getData = $this->db->where('id', $id, 'IN')->get('information');
            } else {
                // If 'id' is a single value, fetch data for that ID
                $getData = $this->db->where('id', $id)->get('information');
            }

            // Check if data retrieval is successful
            if ($getData) {
                return $this->successResponse("GET", $getData);
            } else {
                return $this->failedResponse("GET", "Failed Fetch Request");
            }
        } else {
            // Fetch all data from the database
            $getData = $this->db->get('information');

            // Check if data retrieval is successful
            if ($getData) {
                return $this->successResponse("GET", $getData);
            } else {
                return $this->failedResponse("GET", "Failed Fetch Request");
            }
        }
    }


    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {
        // Define the required fields
        $requiredFields = ['first_name', 'middle_name', 'last_name', 'contact_number'];

        // Check if payload is an array and not empty
        if (!is_array($payload) || empty($payload)) {
            return $this->failedResponse("POST", "Invalid or Empty Payload");
        }

        // Check if all required fields are present
        $missingFields = array_diff($requiredFields, array_keys($payload));
        if (!empty($missingFields)) {
            return $this->failedResponse("POST", "Missing required fields");
        }

        // Try to insert data into the database
        $insertData = $this->db->insert('information', $payload);

        // Check if data insertion was successful
        if ($insertData) {
            // Fetch the inserted data from the database
            $insertedRecord = $this->db->where('id', $insertData)->getOne('information');
            
            if ($insertedRecord) {
                return $this->successResponse("POST", $insertedRecord);
            } else {
                return $this->failedResponse("POST", "Failed to retrieve inserted data");
            }
        } else {
            return $this->failedResponse("POST", "Failed to Insert Data");
        }
    }


    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
        // Check if param is valid
        if (empty($id) || empty($payload)) {
            return $this->failedResponse("PUT", "Empty ID or Payload");
        }
        
        // Check if ID in payload matches the ID in the URL
        if ($payload['id'] !== intval($id)) {
            return $this->failedResponse("PUT", "ID mismatch between payload and URL");
        }

        // Check if the ID exists in the database
        $existingRecord = $this->db->where('id', $id)->getOne('information');

        // Specify the ID in the update condition and tries to update data
        $this->db->where('id', $id);
        $updateData = $this->db->update('information', $payload);

        //Check if update is successful
        if ($updateData and $existingRecord) {
            $updatedRecord = $this->db->where('id', $id)->getOne('information');
            return $this->successResponse("PUT", $updatedRecord);
        } else {
            return $this->failedResponse("PUT", "Failed to Update the Data");
        }
    }

    /**
    * HTTP DELETE Request
    *
    * @param $id
    * @param $payload
    */
    public function httpDelete($id, $payload)
    {
        // Check if param is valid
        if (empty($id) || empty($payload)) {
            return $this->failedResponse("DELETE", "Empty ID or Payload");
        }

        // Converts id to int or an array of int to prepare for comparison with the payload
        if (strpos($id, ',') !== false) {
            $id = array_map('intval', explode(',', $id));
        } else {
            $id = intval($id);
        }

        // Check if ID in payload matches the IDs in the URL
        if ($payload['id'] !== $id) {
            return $this->failedResponse("DELETE", "ID mismatch between payload and URL");
        }

        // Check if the ID exists in the database
        $existingRecord = $this->db->where('id', $id)->getOne('information');
        
        // Determine if it's a single or multiple deletion
        if (is_array($payload['id'])) {
            // If payload contains an array of IDs, use SQL IN condition (multi record deletion)
            $this->db->where('id', $payload['id'], 'IN');
        } else {
            // If not an array, use where() with ID condition (single record deletion)
            $this->db->where('id', $id);
        }

        // Delete the data
        $deleteData = $this->db->delete('information');

        //Check if the delete was successful
        if ($deleteData and $existingRecord) {
            return $this->successResponse("DELETE", $payload['id']);
        } else {
            return $this->failedResponse("DELETE", "Failed to Delete Data");
        }
    }





    // Success Response
    private function successResponse($method, $data)
    {
        $response = array(
            'method' => $method,
            'status' => 'success',
            'data' => $data
        );
        return json_encode($response);
    }

    // Failed Response
    private function failedResponse($method, $message)
    {
        $response = array(
            'method' => $method,
            'status' => 'failed',
            'message' => $message
        );
        return json_encode($response);
    }
}


// Check if $_SERVER['REQUEST_METHOD'] is set before trying to access it
$request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';

// For GET,POST,PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];


        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));


        $last_index = count($exploded_request_uri) - 1;


        $ids = $exploded_request_uri[$last_index];
    }
}

//payload data
$received_data = json_decode(file_get_contents('php://input'), true);

$api = new API;


//Checking if what type of request and designating to specific functions
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}

?>


