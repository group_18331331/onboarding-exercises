<?php

require_once 'API.php';

use PHPUnit\Framework\TestCase;


class APITestSuccess extends TestCase
{
    private $api;
    
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testHttpGet()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $payload = array('id' => 1);

        $result = json_decode($this->api->httpGet($payload), true);

        // Result must have a status and its value is success
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');

        // Checks if result data is not empty or null
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']);

        foreach ($result['data'] as $dataItem) {

            // Assert that specific values are not empty or null
            $this->assertArrayHasKey('id', $dataItem);
            $this->assertNotEmpty($dataItem['id']);
    
            $this->assertArrayHasKey('middle_name', $dataItem);
            $this->assertNotEmpty($dataItem['middle_name']);

            $this->assertArrayHasKey('last_name', $dataItem);
            $this->assertNotEmpty($dataItem['last_name']);
    
            $this->assertArrayHasKey('contact_number', $dataItem);
            $this->assertNotEmpty($dataItem['contact_number']);
    

            // Checks if the expected keys exists in the result
            $expected_keys = ['id',
                            'first_name',
                            'middle_name',
                            'last_name',
                            'contact_number'];
            $this->assertEquals($expected_keys, array_keys($dataItem));
        }

        $this->assertEquals($expected_keys, array_keys($result['data'][0]));
    }

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';


        $payload = array(
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );
        
        $result = json_decode($this->api->httpPost($payload), true);

        // Result must have a status and its value is success
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');

        // Checks if result data is not empty or null
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']);

        // Extract the first (and only) data item from the response
        $dataItem = $result['data'];

        // Assert that specific values are not empty or null
        $this->assertArrayHasKey('id', $dataItem);
        $this->assertNotEmpty($dataItem['id']);

        $this->assertArrayHasKey('first_name', $dataItem);
        $this->assertNotEmpty($dataItem['first_name']);

        $this->assertArrayHasKey('middle_name', $dataItem);
        $this->assertNotEmpty($dataItem['middle_name']);

        $this->assertArrayHasKey('last_name', $dataItem);
        $this->assertNotEmpty($dataItem['last_name']);

        $this->assertArrayHasKey('contact_number', $dataItem);
        $this->assertNotEmpty($dataItem['contact_number']);

        // Checks if the expected keys exist in the result
        $expected_keys = ['id', 'first_name', 'middle_name', 'last_name', 'contact_number'];
        $this->assertEquals($expected_keys, array_keys($dataItem));        
    }

    public function testHttpPut()
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $id = 1;
        $payload = array(
            'id' => $id,
            'first_name' => 'Updated',
            'middle_name' => 'Updated',
            'last_name' => 'Updated',
            'contact_number' => 987654
        );

        $result = json_decode($this->api->httpPut($id, $payload), true);

        // Result must have a status and its value is success
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');

        // Checks if result data is not empty or null
        $this->assertArrayHasKey('data', $result);
        $this->assertNotEmpty($result['data']);

        // Define the expected keys
        $expected_keys = ['id', 'first_name', 'middle_name', 'last_name', 'contact_number'];
    
        // Check if the expected keys exist in the result
        $this->assertEquals($expected_keys, array_keys($result['data']));
    }

    public function testHttpDelete()
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $id = '6'; 

        // Prepare a payload with the expected structure 
        $payload = array(
            'id' => 6 
        );

        $result = json_decode($this->api->httpDelete($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

}

?>
