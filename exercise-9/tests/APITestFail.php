<?php

require_once 'API.php';

use PHPUnit\Framework\TestCase;


class APITestFail extends TestCase 
{
    private $api;

    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testHttpGetFail()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        // Id does not exist in database
        $payload = array('id' => 99999);

        $result = json_decode($this->api->httpGet($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'Failed Fetch Request');       
    }

    public function testHttpGetInvalidParam()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';
        
        // Invalid payloaf format
        $payload = 'invalid_payload';

        $result = json_decode($this->api->httpGet($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'Invalid Payload Format'); 

    }

    public function testHttpPostFail()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';
    
        // If ID is identified and it already exists
        $payload = array(
            'id' => 1,
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );

        $result = json_decode($this->api->httpPost($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'Failed to Insert Data');

    }

    public function testHttpPostInvalidParam()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';
    
        $payload = 'invalid_payload';

        $result = json_decode($this->api->httpPost($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'Invalid or Empty Payload');

    }

    public function testHttpPostMissingParams()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        // Missing fields
        $payload = array( 
            'last_name' => 'last test',
            'contact_number' => 654655
        );
        
        $result = json_decode($this->api->httpPost($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'Missing required fields');
    }

    public function testHttpPutFail()
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';
        
        // ID does not exist
        $id = 9999;
        $payload = array(
            'id' => 9999,
            'first_name' => 'Test mismatch',
            'middle_name' => 'Test mismatch',
            'last_name' => 'Test mismatch',
            'contact_number' => 987654
        );

        $result = json_decode($this->api->httpPut($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], "Failed to Update the Data");
    }

    public function testHttpPutEmptyParam()
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';
    
        // Valid ID
        $id = 1;

        // Empty payload
        $payload = array();


        $result = json_decode($this->api->httpPut($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'Empty ID or Payload');
    }

    public function testHttpPutMismatch()
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';
    
        $id = 1;
        $payload = array(
            'id' => 5,
            'first_name' => 'Test mismatch',
            'middle_name' => 'Test mismatch',
            'last_name' => 'Test mismatch',
            'contact_number' => 987654
        );

        $result = json_decode($this->api->httpPut($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'ID mismatch between payload and URL');
    }

    public function testHttpDeleteFail()
    {
        $_SERVER['REQUEST_METHOD'] = 'Delete';
        
        // ID does not exist
        $id = 9999;
        $payload = array(
            'id' => 9999,
            'first_name' => 'Test mismatch',
            'middle_name' => 'Test mismatch',
            'last_name' => 'Test mismatch',
            'contact_number' => 987654
        );

        $result = json_decode($this->api->httpDelete($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], "Failed to Delete Data");
    }

    public function testHttpDeleteEmptyParam()
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';
    
        // Valid ID
        $id = 1;

        // Empty payload
        $payload = array();


        $result = json_decode($this->api->httpDelete($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'Empty ID or Payload');
    }

    public function testHttpDeleteMismatch()
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';
    
        $id = 1;
        $payload = array(
            'id' => 5,
            'first_name' => 'Test mismatch',
            'middle_name' => 'Test mismatch',
            'last_name' => 'Test mismatch',
            'contact_number' => 987654
        );

        $result = json_decode($this->api->httpDelete($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');

        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], 'ID mismatch between payload and URL');
    }
}

?>