<?php

require_once 'API.php';

use PHPUnit\Framework\TestCase;


class APITest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testHttpGet()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $payload = array('id' => 1);

        $result = json_decode($this->api->httpGet($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';


        $payload = array(
            'id' => 8,
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );
        
        $result = json_decode($this->api->httpPost($payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    public function testHttpPut()
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $id = 1;
        $payload = array(
            'id' => $id,
            'first_name' => 'Updated',
            'middle_name' => 'Updated',
            'last_name' => 'Updated',
            'contact_number' => 987654
        );

        $result = json_decode($this->api->httpPut($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    public function testHttpDelete()
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $id = '6'; 

        // Prepare a payload with the expected structure 
        $payload = array(
            'id' => 6 
        );

        $result = json_decode($this->api->httpDelete($id, $payload), true);

        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

}

?>