<?php

// To access MysqliDb.php
require 'vendor/autoload.php';

// Start session
session_start();


// Connect to database
function connectToDatabase () {
  $host = 'localhost';
  $username = 'root';
  $password = '';
  $database = 'p8';

  $db = new MysqliDb($host, $username, $password, $database);

  if ($db->ping()) {
    return $db;
  } else {
    die('Database connection failed: ' . $db->getLastError());
  }
}

// Function to fetch all employees
function getAllEmployees() {
    $db = connectToDatabase();
    return $db->get('employee');
}

// Add a new employee record
function addEmployee($first_name, $last_name, $middle_name, $birthday, $address)
{
    $db = connectToDatabase();

    $data = array(
        'first_name' => $first_name,
        'last_name' => $last_name,
        'middle_name' => $middle_name,
        'birthday' => $birthday,
        'address' => $address,
    );

    // Checks if employee exists before adding
    $existingEmployee = $db->where('first_name', $first_name)
                           ->where('last_name', $last_name)
                           ->where('middle_name', $middle_name)
                           ->getOne('employee');
    
    if ($existingEmployee) {
        $_SESSION['message'] = "Employee already exists";
    } elseif ($db->insert('employee', $data)) {
        $_SESSION['message'] = "New record created successfully";
    } else {
        $_SESSION['message'] = "Error: " . $db->getLastError();
    }

    $db->disconnect();
}

// Update an employee's address
function updateEmployeeAddress($employee_id, $new_address)
{
    $db = connectToDatabase();

    $data = array('address' => $new_address);

    // Updates only if employee exists
    $existingEmployee = $db->where('id', $employee_id)
                           ->getOne('employee');
    
    if ($existingEmployee) {
        $db->where('id', $employee_id);
        if ($db->update('employee', $data)) {
        $_SESSION['message'] = "Record updated successfully";
        } else {
            $_SESSION['message'] = "Error updating record: " . $db->getLastError();
        }
    } else {
        $_SESSION['message'] = "Employee does not exist";
    }

    $db->disconnect();
}

// Function to delete an employee based on ID
function deleteEmployeeById($employee_id)
{
    $db = connectToDatabase();

    $existingEmployee = $db->where('id', $employee_id)
                           ->getOne('employee');

    // Deletes only if employee exists
    if ($existingEmployee) {
        $db->where('id', $employee_id);
        if ($db->delete('employee')) {
            $_SESSION['message'] = "Employee deleted successfully";
        } else {
            $_SESSION['message'] = "Error deleting employee: " . $db->getLastError();
        }
    } else {
        $_SESSION['message'] = "Employee does not exist";
    }

    $db->disconnect();
}



// Fetch all employees
$employees = getAllEmployees();

// Check for form submissions and handle them
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // Add an employee
    if (isset($_POST['add_employee'])) {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $middle_name = $_POST['middle_name'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];

        addEmployee($first_name, $last_name, $middle_name, $birthday, $address);
    }

    // Update address of an employee
    if (isset($_POST['update_address'])) {
        $employee_id = $_POST['employee_id'];
        $new_address = $_POST['new_address'];

        updateEmployeeAddress($employee_id, $new_address);
    }

    // Delete an employee
    if (isset($_POST['delete_employee'])) {
        $employee_id = $_POST['employee_id_del'];

        deleteEmployeeById($employee_id);
    }

    // Redirect to avoid form resubmission
    header("Location: exercise-8.php");
    exit();
}

// Display messages to the user if available
if (isset($_SESSION['message'])) {
    echo $_SESSION['message'] . "<br>";
    unset($_SESSION['message']); //Clear message after displaying it
}
?>

<!-- Page Title -->
<h1>Create Simple CRUD</h1>

<!-- Display all employees -->
<h2>All Employees:</h2>
<table border="1">
  <tr>
    <th>ID</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Middle Name</th>
    <th>Birthday</th>
    <th>Address</th>
  </tr>
  <?php foreach ($employees as $employee): ?>
    <tr>
      <td><?= $employee['id'] ?></td>
      <td><?= $employee['first_name'] ?></td>
      <td><?= $employee['last_name'] ?></td>
      <td><?= $employee['middle_name'] ?></td>
      <td><?= $employee['birthday'] ?></td>
      <td><?= $employee['address'] ?></td>
    </tr>
  <?php endforeach; ?>
</table>

<!-- HTML form for creating a new employee record -->
<h2>Add New Employee:</h2>
<form method="POST">
  <label for="first_name">First Name:</label>
  <input type="text" id="first_name" name="first_name" required><br>

  <label for="last_name">Last Name:</label>
  <input type="text" id="last_name" name="last_name" required><br>

  <label for="middle_name">Middle Name:</label>
  <input type="text" id="middle_name" name="middle_name"><br>

  <label for="birthday">Birthday:</label>
  <input type="date" id="birthday" name="birthday" required><br>

  <label for="address">Address:</label>
  <input type="text" id="address" name="address" required><br>

  <button type="submit" name="add_employee">Add Employee</button>
</form>

<!-- HTML form for updating an employee's address -->
<h2>Update Employee Address:</h2>
<form method="POST">
  <label for="employee_id">Employee ID:</label>
  <input type="number" id="employee_id" name="employee_id" required><br>

  <label for="new_address">New Address:</label>
  <input type="text" id="new_address" name="new_address" required><br>

  <button type="submit" name="update_address">Update Address</button>
</form>

<!-- HTML form for deleting employees whose last name starts with the letter 'D' -->
<h2>Delete Employee by ID:</h2>
<form method="POST">
  <label for="employee_id_del">Employee ID:</label>
  <input type="number" id="employee_id_del" name="employee_id_del" required><br>

  <button type="submit" name="delete_employee">Delete Employee</button>
</form>

