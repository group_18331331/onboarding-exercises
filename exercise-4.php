<?php

class API {
    public function printFullName($fullName) {
        echo "Full Name: " . $fullName . "<br>";
    }

    public function printHobbies($hobbies) {
        echo "Hobbies:<br>";
        foreach ($hobbies as $hobby) {
            echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $hobby . "<br>";
        }
    }

    public function printPersonalInfo($personalInfo) {
        echo "Age: " . $personalInfo->age . "<br>";
        echo "Email: " . $personalInfo->email . "<br>";
        echo "Birthday: " . $personalInfo->birthday . "<br>";
    }
}

// Instance of the API class
$api = new API();

// Calling functions with the provided parameters
$api->printFullName("Micha S. Caldaira");

$hobbies = array(
    "Watching Series",
    "Playing Games",
    "Playing Guitar"
);
$api->printHobbies($hobbies);

$personalInfo = (object) array(
    "age" => 20,
    "email" => "michacaldaira@gmail.com",
    "birthday" => "November 21, 2002"
);
$api->printPersonalInfo($personalInfo);

?>
