<?php
//Start the session
session_start();

//Connect to the database
function connectToDatabase(){
  $host = 'localhost';
  $username = 'root';
  $password = '';
  $databse = 'p8';

  $conn = new mysqli($host, $username, $password, $databse);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  return $conn;
}

// Add a new employee record
function addEmployee($first_name, $last_name, $middle_name, $birthday, $address){
  $conn = connectToDatabase();

  $sql_insert = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address)
                 VALUES ('$first_name', '$last_name', '$middle_name', '$birthday', '$address')";
  
  if($conn->query($sql_insert) === TRUE) {
    $_SESSION['message'] = "New record created successfully";
  } else {
    $_SESSION['message'] = "Error: " . $sql_insert . "<br>" . $conn->error;
  }

  $conn->close();
}

// Update an employee's address
function updateEmployeeAddress($employee_id, $new_address) {
  $conn = connectToDatabase();

  $sql_update = "UPDATE employee SET address = '$new_address' WHERE id = $employee_id";

  if ($conn->query($sql_update) === TRUE) {
    $_SESSION['message'] = "Record updated successfully";
  } else {
    $_SESSION['message'] = "Error updating record: " . $conn->error;
  }

  $conn->close();
}

// Function to delete an employee based on ID
function deleteEmployeeById($employee_id) {
  $conn = connectToDatabase();

  $sql_delete = "DELETE FROM employee WHERE id = $employee_id";

  if ($conn->query($sql_delete) === TRUE) {
    $_SESSION['message'] = "Employee deleted successfully";
  } else {
    $_SESSION['message'] = "Error deleting employee: " . $conn->error;
  }

  $conn->close();
}


//Check for form submissions and handle them
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

  // Add an employee
  if (isset($_POST['add_employee'])) {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $middle_name = $_POST['middle_name'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    addEmployee($first_name, $last_name, $middle_name, $birthday, $address);
  }

  // Update address of an employee
  if (isset($_POST['update_address'])) {
    $employee_id = $_POST['employee_id'];
    $new_address = $_POST['new_address'];

    updateEmployeeAddress($employee_id, $new_address);
  }

  // Delete an employee
  if (isset($_POST['delete_employee'])) {
    $employee_id = $_POST['employee_id_del'];

    deleteEmployeeById($employee_id);
  }

  // Redirect to avoid form resubmission
  header("Location: exercise-7.php");
  exit();
}


// Display messages to the user if available
if (isset($_SESSION['message'])) {
  echo $_SESSION['message'] . "<br>";
  unset($_SESSION['message']);      //Clear message after displaying it
}
?>


<!-- HTML form for creating a new employee record -->
<h2>Add New Employee:</h2>
<form method="POST">
  <label for="first_name">First Name:</label>
  <input type="text" id="first_name" name="first_name"><br>

  <label for="last_name">Last Name:</label>
  <input type="text" id="last_name" name="last_name"><br>

  <label for="middle_name">Middle Name:</label>
  <input type="text" id="middle_name" name="middle_name"><br>

  <label for="birthday">Birthday:</label>
  <input type="date" id="birthday" name="birthday"><br>

  <label for="address">Address:</label>
  <input type="text" id="address" name="address"><br>

  <button type="submit" name="add_employee">Add Employee</button>
</form>

<!-- HTML form for updating an employee's address -->
<h2>Update Employee Address:</h2>
<form method="POST">
  <label for="employee_id">Employee ID:</label>
  <input type="number" id="employee_id" name="employee_id"><br>

  <label for="new_address">New Address:</label>
  <input type="text" id="new_address" name="new_address"><br>

  <button type="submit" name="update_address">Update Address</button>
</form>

<!-- HTML form for deleting employee by id -->
<h2>Delete Employee by ID:</h2>
<form method="POST">
  <label for="employee_id_del">Employee ID:</label>
  <input type="number" id="employee_id_del" name="employee_id_del"><br>

  <button type="submit" name="delete_employee">Delete Employee</button>
</form>