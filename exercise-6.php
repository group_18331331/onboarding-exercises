<?php
// Connect to the database using MySQLi
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8';
$conn = new mysqli($host, $username, $password, $database);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Insert data into the employee table
$sql_insert = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address)
               VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";

if ($conn->query($sql_insert) === TRUE) {
    echo "New record created successfully" . "<br>";
} else {
    echo "Error: " . $sql_insert . "<br>" . $conn->error;
}



// Retrieve the first name, last name, and birthday of all employees in the table
$sql_retrieve_all = "SELECT first_name, last_name, birthday FROM employee";
$result_all = $conn->query($sql_retrieve_all);

if ($result_all->num_rows > 0) {
    while ($row = $result_all->fetch_assoc()) {
        echo "First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Birthday: " . $row["birthday"] . "<br>";
    }
} else {
    echo "0 results";
}



// Retrieve the number of employees whose last name starts with the letter 'D'
$sql_retrieve_with_d = "SELECT COUNT(*) AS num_employees_with_d_last_name FROM employee WHERE last_name LIKE 'D%'";
$result_with_d = $conn->query($sql_retrieve_with_d);

if ($result_with_d->num_rows > 0) {
    $row = $result_with_d->fetch_assoc();
    echo "Number of employees with last name starting with 'D': " . $row["num_employees_with_d_last_name"] . "<br>";

} else {
    echo "0 results";
}



// Retrieve the first name, last name, and address of the employee with the highest ID number
$sql_retrieve_highest_id = "SELECT first_name, last_name, address FROM employee WHERE id = (SELECT MAX(id) FROM employee)";
$result_highest_id = $conn->query($sql_retrieve_highest_id);

if ($result_highest_id->num_rows > 0) {
    while ($row = $result_highest_id->fetch_assoc()) {
        echo "First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Address: " . $row["address"] . "<br>";
    }
} else {
    echo "0 results";
}



// Update data in the employee table for the employee with the first name 'John'
$sql_update = "UPDATE employee SET address = '123 Main Street' WHERE first_name = 'John'";

if ($conn->query($sql_update) === TRUE) {
    echo "Record updated successfully" . "<br>";
} else {
    echo "Error updating record: " . $conn->error;
}



// Delete all employees whose last name starts with the letter 'D'
$sql_delete = "DELETE FROM employee WHERE last_name LIKE 'D%'";

if ($conn->query($sql_delete) === TRUE) {
    echo "Records deleted successfully" . "<br>";
} else {
    echo "Error deleting records: " . $conn->error;
}

$conn->close();
?>
